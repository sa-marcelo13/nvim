require("lazy").setup({
	"gruvbox-community/gruvbox",
	{
		"catppuccin/nvim",
		name = "catppuccin",
		priority = 1000,
	},

	{
		"folke/tokyonight.nvim",
		lazy = false,
		priority = 1000,
	},

	{
		"nvim-tree/nvim-tree.lua",
		version = "*",
		lazy = false,
		dependencies = {
			"nvim-tree/nvim-web-devicons",
		},
	},

	"kyazdani42/nvim-web-devicons",

	{
		"windwp/nvim-autopairs",
		event = "InsertEnter",
	},

	{
		"numToStr/Comment.nvim",
		lazy = false,
		opts = {
			config = true,
		},
	},

	{
		"ntpeters/vim-better-whitespace",
		lazy = false,
	},

	{
		"mhinz/vim-signify",
		lazy = false,
	},

	"akinsho/bufferline.nvim",

	"nvim-lualine/lualine.nvim",

	"nvim-lua/plenary.nvim",
	"nvim-telescope/telescope.nvim",
	"nvim-telescope/telescope-ui-select.nvim",
	"nvim-telescope/telescope-dap.nvim",
	{
		"nvim-telescope/telescope-fzf-native.nvim",
		build = "cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build",
	},

	{
		"nvim-treesitter/nvim-treesitter",
		run = function()
			require("nvim-treesitter.install").update({ with_sync = true })
		end,
	},

	"neovim/nvim-lspconfig",
	{
		"hrsh7th/cmp-nvim-lsp",
		lazy = false,
		dependencies = {
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-path",
			"hrsh7th/cmp-cmdline",
			"hrsh7th/nvim-cmp",
			"L3MON4D3/LuaSnip",
		},
	},

	"ray-x/lsp_signature.nvim",
	"https://git.sr.ht/~whynothugo/lsp_lines.nvim",

	"folke/neodev.nvim",

	"mfussenegger/nvim-dap",
	"mfussenegger/nvim-dap-python",
	"leoluz/nvim-dap-go",
	"rcarriga/nvim-dap-ui",
	{
		"theHamsta/nvim-dap-virtual-text",
		lazy = false,
	},
	{
		"mfussenegger/nvim-jdtls",
		lazy = false,
	},
	{
		"mrcjkb/rustaceanvim",
		lazy = false,
	},

	{
		"nvim-neotest/neotest",
		dependencies = {
			"nvim-neotest/nvim-nio",
			"nvim-lua/plenary.nvim",
			"antoinemadec/FixCursorHold.nvim",
			"nvim-treesitter/nvim-treesitter",
		},
	},

	"jose-elias-alvarez/null-ls.nvim",

	"williamboman/mason.nvim",
	"williamboman/mason-lspconfig.nvim",

	--terminal
	"akinsho/toggleterm.nvim",
}, {
	defaults = { lazy = true },
	install = { colorscheme = { "tokyonight" } },
	checker = { enabled = true },
	change_detection = {
		notify = true,
	},
	performance = {
		rtp = {
			disabled_plugins = {
				"gzip",
				"matchit",
				"matchparen",
				"tarPlugin",
				"tohtml",
				"tutor",
				"zipPlugin",
			},
		},
	},
})
