require("nvim-tree").setup({
	sort_by = "case_sensitive",
	view = {
		-- adaptive_size = true,
		number = true,
		relativenumber = true,
		width = 40,
	},
	renderer = {
		group_empty = true,
	},
	filters = {
		dotfiles = false,
	},
})
