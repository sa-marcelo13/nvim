require("lualine").setup({
	options = {
		theme = "nightfly",
		section_separators = {
			left = "",
			right = "",
		},
		component_separators = {
			left = "",
			right = "",
		},
	},
})
