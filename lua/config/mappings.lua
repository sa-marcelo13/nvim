local bufopts = { noremap = true, silent = true, buffer = bufnr }

vim.keymap.set("n", "<leader>q", "<cmd>bp!<bar>sp!<bar>bn!<bar>bd!<cr>", bufopts)
vim.keymap.set("n", "<leader>ex", "<cmd>bp!<cr>", bufopts)
vim.keymap.set("n", "<leader>C", "<cmd>%bd<bar>e#<bar>bd#<bar>NvimTreeOpen<cr>", bufopts)
vim.keymap.set("n", "<leader>/", "<cmd>noh<cr>", bufopts)
vim.keymap.set("n", "<leader>A", "gg<bar>V<bar>G<bar><cr>", bufopts)
vim.keymap.set("n", "<leader>te", "<cmd>q<bar>q<cr>", bufopts)

-- tab
vim.keymap.set(
	"n",
	"<leader>t2",
	"<cmd>set shiftwidth=2<cr> <cmd> set tabstop=2<cr> <cmd>set softtabstop=2<cr>",
	bufopts
)

--nvimtree
vim.keymap.set("n", "<leader>a", "<cmd>NvimTreeToggle<cr>", bufopts)

--splits
vim.keymap.set("", "<C-h>", "<C-w>h", bufopts)
vim.keymap.set("", "<C-j>", "<C-w>j", bufopts)
vim.keymap.set("", "<C-k>", "<C-w>k", bufopts)
vim.keymap.set("", "<C-l>", "<C-w>l", bufopts)
vim.keymap.set("", "<Up>", "<cmd>resize -5<cr>", bufopts)
vim.keymap.set("", "<Down>", "<cmd>resize +5<cr>", bufopts)
vim.keymap.set("", "<Right>", "<cmd>vertical resize -5<cr>", bufopts)
vim.keymap.set("", "<Left>", "<cmd>vertical resize +5<cr>", bufopts)

--bufferline
vim.keymap.set("n", "bn", "<cmd>BufferLineCycleNext<cr>", bufopts)
vim.keymap.set("n", "bp", "<cmd>BufferLineCyclePrev<cr>", bufopts)

vim.keymap.set("n", "<leader>mn", "<cmd>BufferLineMoveNext<cr>", bufopts)
vim.keymap.set("n", "<leader>mp", "<cmd>BufferLineMovePrev<cr>", bufopts)

vim.keymap.set("n", "<leader>se", "<cmd>BufferLineSortByExtension<cr>", bufopts)
vim.keymap.set("n", "<leader>sd", "<cmd>BufferLineSortByDirectory<cr>", bufopts)
vim.keymap.set("n", "<leader>st", "<cmd>BufferLineSortByTabs<cr>", bufopts)

vim.keymap.set("n", "<leader>cr", "<cmd>BufferLineCloseRight<cr>", bufopts)
vim.keymap.set("n", "<leader>cl", "<cmd>BufferLineCloseLeft<cr>", bufopts)

vim.keymap.set("n", "<leader>1", "<cmd>BufferLineGoToBuffer 1<cr>", bufopts)
vim.keymap.set("n", "<leader>2", "<cmd>BufferLineGoToBuffer 2<cr>", bufopts)
vim.keymap.set("n", "<leader>3", "<cmd>BufferLineGoToBuffer 3<cr>", bufopts)
vim.keymap.set("n", "<leader>4", "<cmd>BufferLineGoToBuffer 4<cr>", bufopts)
vim.keymap.set("n", "<leader>5", "<cmd>BufferLineGoToBuffer 5<cr>", bufopts)
vim.keymap.set("n", "<leader>6", "<cmd>BufferLineGoToBuffer 6<cr>", bufopts)
vim.keymap.set("n", "<leader>7", "<cmd>BufferLineGoToBuffer 7<cr>", bufopts)
vim.keymap.set("n", "<leader>8", "<cmd>BufferLineGoToBuffer 8<cr>", bufopts)
vim.keymap.set("n", "<leader>9", "<cmd>BufferLineGoToBuffer 9<cr>", bufopts)
vim.keymap.set("n", "<leader>$", "<cmd>BufferLineGoToBuffer -1<cr>", bufopts)

--toggleterm
vim.keymap.set("n", "<leader>tt", "<cmd>ToggleTerm<cr>", bufopts)

--git
vim.keymap.set("n", "<leader>gd", "<cmd>SignifyDiff<cr>", bufopts)
vim.keymap.set("n", "<leader>gl", "<cmd>SignifyList<cr>", bufopts)
vim.keymap.set("n", "<leader>gr", "<cmd>SignifyHunkUndo<cr>", bufopts)

--telescope
vim.keymap.set("n", "<leader>ff", "<cmd>Telescope find_files<cr>", bufopts)
vim.keymap.set("n", "<leader>fg", "<cmd>Telescope live_grep<cr>", bufopts)
-- vim.keymap.set('n', '<leader>fm', '<cmd>Telescope media_files<cr>', bufopts)
vim.keymap.set("n", "<leader>fb", "<cmd>Telescope buffers<cr>", bufopts)
vim.keymap.set("n", "<leader>fh", "<cmd>Telescope help_tags<cr>", bufopts)

--lsp
vim.keymap.set("n", "gD", "<cmd>Telescope lsp_declarations<cr>", bufopts)
vim.keymap.set("n", "gd", "<cmd>Telescope lsp_definitions<cr>", bufopts)
vim.keymap.set("n", "gr", "<cmd>Telescope lsp_references<cr>", bufopts)
vim.keymap.set("n", "gi", "<cmd>Telescope lsp_implementations<cr>", bufopts)

-- vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
-- vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
-- vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
-- vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)

vim.keymap.set("n", "K", vim.lsp.buf.hover, bufopts)
vim.keymap.set("n", "<leader>k", vim.lsp.buf.signature_help, bufopts)
vim.keymap.set("n", "<leader>wa", vim.lsp.buf.add_workspace_folder, bufopts)
vim.keymap.set("n", "<leader>wr", vim.lsp.buf.remove_workspace_folder, bufopts)
vim.keymap.set("n", "<leader>wl", function()
	print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
end, bufopts)
vim.keymap.set("n", "<leader>D", vim.lsp.buf.type_definition, bufopts)
vim.keymap.set("n", "rn", vim.lsp.buf.rename, bufopts)
vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, bufopts)
vim.keymap.set("n", "<leader>fm", "<cmd>lua vim.lsp.buf.format()<cr>", bufopts)

--jdtls
vim.keymap.set("n", "<leader>o", "<cmd>lua require'jdtls'.organize_imports()<cr>", bufopts)
vim.keymap.set("n", "<leader>v", "<cmd>lua require'jdtls'.extract_variable()<cr>", bufopts)
vim.keymap.set("n", "<leader>c", "<cmd>lua require'jdtls'.extract_constant()<cr>", bufopts)
vim.keymap.set("", "<leader>jr", "<cmd>JdtRestart<cr>", bufopts)

--compile gradle kotlin
vim.keymap.set("n", "<leader>kc", "<cmd>terminal gradle build<cr>", bufopts)

--dap
vim.keymap.set("n", "<leader>tb", "<cmd>lua require'dap'.toggle_breakpoint()<cr>", bufopts)
vim.keymap.set("n", "<F5>", "<cmd>lua require('dapui').open() require'dap'.continue()<cr>", bufopts)
vim.keymap.set("n", "<F7>", "<cmd>lua require'dap'.step_into()<cr>", bufopts)
vim.keymap.set("n", "<F8>", "<cmd>lua require'dap'.step_over()<cr>", bufopts)
vim.keymap.set("n", "<F9>", "<cmd>lua require'dap'.step_out()<cr>", bufopts)
vim.keymap.set("n", "<F10>", "<cmd>lua require'dap'.run_last()<cr>", bufopts)
vim.keymap.set("n", "<leader>dr", "<cmd>lua require'dap'.repl.open()<cr>", bufopts)

--dap-ui
vim.keymap.set("n", "<leader>dc", "<cmd>lua require('dapui').close()<cr>", bufopts)
vim.keymap.set("v", "<leader>ev", "<cmd>lua require('dapui').eval()<cr>", bufopts)

--dap telescope
vim.keymap.set("n", "<leader>dapc", "<cmd>lua require'telescope'.extensions.dap.commands{}<cr>", bufopts)
vim.keymap.set("n", "<leader>daps", "<cmd> lua require'telescope'.extensions.dap.configurations{}<cr>", bufopts)
vim.keymap.set("n", "<leader>dapl", "<cmd> lua require'telescope'.extensions.dap.list_breakpoints{}<cr>", bufopts)
vim.keymap.set("n", "<leader>dapv", "<cmd> lua require'telescope'.extensions.dap.variables{}<cr>", bufopts)
vim.keymap.set("n", "<leader>dapf", "<cmd> lua require'telescope'.extensions.dap.frames{}<cr>", bufopts)

--python unit test
vim.keymap.set("n", "<leader>tm", "<cmd>lua require('dap-python').test_method()<cr>", bufopts)
vim.keymap.set("n", "<leader>tc", "<cmd>lua require('dap-python').test_class()<cr>", bufopts)
vim.keymap.set("v", "<leader>ds", "<cmd><ESC>lua require('dap-python').debug_selection()<cr>", bufopts)

--vim diagnostics
vim.keymap.set("n", "<leader>di", vim.diagnostic.open_float, bufopts)
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, bufopts)
vim.keymap.set("n", "]d", vim.diagnostic.goto_next, bufopts)
vim.keymap.set("n", "<leader>dq", vim.diagnostic.setloclist, bufopts)

-- commentary
vim.keymap.set({ "n", "v", "i" }, "<C-_>", "<cmd>Commentary<cr>", bufopts)
