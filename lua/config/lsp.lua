local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)
local util = require("lspconfig/util")

local lsp_flags = {
	debounce_text_changes = 150,
}

require("lspconfig").clangd.setup({
	capabilities = capabilities,
	flags = lsp_flags,
})

require("lspconfig").pyright.setup({
	capabilities = capabilities,
	flags = lsp_flags,
})

require("lspconfig").bashls.setup({
	capabilities = capabilities,
	flags = lsp_flags,
})

require("lspconfig").terraformls.setup({
	capabilities = capabilities,
	flags = lsp_flags,
})

require("lspconfig").gopls.setup({
	capabilities = capabilities,
	flags = lsp_flags,
	cmd = { "gopls" },
	filetypes = { "go", "gomod", "gowork", "gotmpl" },
	root_dir = util.root_pattern("go.work", "go.mod", ".git"),
	settings = {
		gopls = {
			completeUnimported = true,
			usePlaceholders = true,
			analyses = {
				unusedparams = true,
			},
		},
	},
})

require("lspconfig").tsserver.setup({
	capabilities = capabilities,
	flags = lsp_flags,
	filetypes = {
		"typescript",
		"javascript",
		"typescriptreact",
		"typescript.tsx",
	},
	cmd = {
		"typescript-language-server",
		"--stdio",
	},
})

require("lspconfig").cssls.setup({
	capabilities = capabilities,
	flags = lsp_flags,
})

require("lspconfig").html.setup({
	capabilities = capabilities,
	flags = lsp_flags,
})

require("lspconfig").kotlin_language_server.setup({
	capabilities = capabilities,
	flags = lsp_flags,
})

require("lsp_signature").setup({
	bind = true,
	handler_opts = {
		border = "rounded",
	},
})

vim.diagnostic.config({
	virtual_text = false,
})

require("lsp_lines").setup()

vim.o.completeopt = "menuone,noselect"

local source_mapping = {
	buffer = "◉ Buffer",
	nvim_lsp = "👐 LSP",
	nvim_lua = "🌙 Lua",
	path = "🚧 Path",
	luasnip = "🌜 LuaSnip",
}

local cmp = require("cmp")

cmp.setup({
	preselect = cmp.PreselectMode.None,

	sources = {
		{ name = "nvim_lsp" },
		{ name = "buffer" },
		{ name = "path" },
	},

	window = {
		completion = cmp.config.window.bordered(),
		documentation = cmp.config.window.bordered(),
	},

	snippet = {
		expand = function(args)
			require("luasnip").lsp_expand(args.body)
		end,
	},

	mapping = {
		["<Down>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
		["<Up>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }),
		["<C-p>"] = cmp.mapping.scroll_docs(-1),
		["<C-n>"] = cmp.mapping.scroll_docs(1),
		["<C-e>"] = cmp.mapping.close(),
		["<CR>"] = cmp.mapping.confirm({
			select = false,
		}),

		["<Tab>"] = function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			else
				fallback()
			end
		end,
		["<S-Tab>"] = function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			else
				fallback()
			end
		end,
	},

	formatting = {
		format = function(entry, vim_item)
			local menu = source_mapping[entry.source.name]
			vim_item.menu = menu
			return vim_item
		end,
	},
})

require("luasnip/loaders/from_vscode").load()
