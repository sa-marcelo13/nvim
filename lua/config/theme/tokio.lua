require("tokyonight").setup({
	style = "night",
	light_style = "day",
	transparent = false,
	terminal_colors = true,
	styles = {
		comments = {
			italic = true,
		},
		keywords = {
			italic = true,
		},
		functions = {},
		variables = {},
		sidebars = "dark",
		floats = "dark",
	},
	sidebars = {
		"qf",
		"help",
		"packer",
	},
	day_brightness = 0.3,
	hide_inactive_statusline = false,
	dim_inactive = false,
	lualine_bold = false,

	on_colors = function(colors)
		colors.bg = "#121212"
	end,

	on_highlights = function(highlights, colors) end,
})

vim.cmd.colorscheme("tokyonight-night")
